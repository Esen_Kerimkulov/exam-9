import React, { Component } from 'react';
import Switch from "react-router-dom/es/Switch";
import Layout from "./components/Layout/Layout";
import {Route} from "react-router-dom";
import Dishes from './components/contacts/contact';
import './App.css';
import AddDish from "./components/AddContact/AddContact";
import EditContact from "./components/EditContact/EditContact";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/contacts" component={Dishes}/>
          <Route path="/addContact" component={AddDish}/>
          <Route path="/editDish/:id" component={EditContact}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
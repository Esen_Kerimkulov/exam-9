import axios from '../../axios-contact';
export const ADD_CONTACTS = "ADD_CONTACTS";
export const EDIT_CONTACTS = "EDIT_CONTACTS";
export const REMOVE_CONTACTS = "REMOVE_CONTACTS";
export const GET_CONTACTS = "GET_CONTACTS";

export const addToContact = () => ({type: ADD_CONTACTS});
export const getContactSuccess = (contacts) => ({type: GET_CONTACTS, contacts});

export const getContacts = () => {
    return (dispatch) => {
        return axios.get('contacts.json').then(response => {
            const contacts = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });

            dispatch(getContactSuccess(contacts));
        })
    }
};

export const  postRequest = dishes => {
    return dispatch => {
        axios.post('contacts.json', dishes).then(() => {
            dispatch(getContacts())
        })
    }
};



export const deleteContact = (id) => {
    return dispatch => {
        axios.delete('contacts/' + id + '.json').then(() => {
            this.props.history.push('/contacts')
        })
    }
};
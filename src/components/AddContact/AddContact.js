import React, {Component} from 'react';
import {postRequest} from "../../store/actions/actions";
import {connect} from "react-redux";

import './AddDish.css';

class AddContact extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: '',
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    dishesHandler = () => {
        const dishes = {...this.state};

        this.props.addDishes(dishes);
        this.props.history.push('/contacts')
    };

    render() {
        return (
            <div className="AddContactsForm">
                <label htmlFor="Name">Name:</label>
                <input value={this.state.name} name="name"
                       onChange={this.valueChanged}
                       id="Title" type="text"
                />
                <label htmlFor="Phone">Phone:</label>
                <input value={this.state.phone} name="phone"
                       onChange={this.valueChanged}
                       id="Phone" type="text"
                />
                <label htmlFor="Email">Email:</label>
                <input value={this.state.email} name="email"
                       onChange={this.valueChanged}
                       id="Email" type="text"
                />
                <label htmlFor="Photo">Photo: </label>
                <input value={this.state.photo} name="photo"
                       onChange={this.valueChanged}
                       id="Photo" type="text"
                />
                <button className="butn" onClick={this.dishesHandler}>Add To Menu</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.loading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addDishes: (dishes) => dispatch(postRequest(dishes))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddContact);
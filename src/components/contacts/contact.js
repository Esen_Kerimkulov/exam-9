import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deleteContact, getContacts} from '../../store/actions/actions';
import './contact.css';




class Contact extends Component {

    componentDidMount() {
        this.props.getContacts().then(() => {
        })
    };

    addNewContact = () => {
        this.props.history.push('/AddContact')
    };

    editContact = (id) => {
        this.props.history.push(`/editContact/${id}`)
    };

    render() {
        return (
            <div className="Conts">
                <div className="Contacts">
                    <h1>Contacts</h1>
                    <button className="btn" onClick={this.addNewContact}>Add New Contact</button>
                </div>
                {this.props.contacts.map((contacts) => {
                    return (
                        <div className="Dish" key={contacts.id}>
                            <button className="edit" onClick={() => this.editContact(contacts.id)}>edit</button>
                            <button className="delete" onClick={() => this.props.deleteContact(contacts.id)}>delete</button>
                            <img className="img" alt='image' src={contacts.photo} />
                            <p>{contacts.name}</p>
                            <p><span><br/>Phone: </span>{contacts.phone}</p>
                        </div>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getContacts: () => dispatch(getContacts()),
        deleteContact: (id) => dispatch(deleteContact(id)),
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(Contact);
import React, {Component, Fragment} from 'react';
import axios from 'axios';

import './EditContact.css'

class EditContact extends Component {

  state = {
    name: '',
    phone: '',
    email: '',
    photo: '',
  };

  componentDidMount() {
    axios.get(`https://exam-9-esen.firebaseio.com/contacts/${this.props.match.params.id}.json`).then(response => {
      this.setState({
        name: response.data.name,
        phone: response.data.phone,
        email: response.data.email,
        photo: response.data.photo,
      })
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.state.id !== prevState.id) {
      axios.get(`https://exam-9-esen.firebaseio.com/contacts/${this.props.match.params.id}.json`).then(response => {
        this.setState({
          name: response.data.name,
          phone: response.data.phone,
          email: response.data.email,
          photo: response.data.photo,
        })
      })
    }
  }

  changeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  editPage = (event) => {
    event.preventDefault();
    const data = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      photo: this.state.photo,
    };
    axios.put(`https://exam-9-esen.firebaseio.com/contacts/${this.props.match.params.id}.json`, data).then(() => {
      this.props.history.replace('/contacts');
    });
  };

  dishesHandler = () => {
    const dishes = {...this.state};


    this.props.addDishes(dishes);
    this.props.history.push('/contacts')
  };

  render() {
    return (
        <Fragment>
          <h2>Edit Dishes</h2>
          <form onSubmit={this.editPage}>
            <div className="EditForm">
              <label htmlFor="Name">Name:</label>
              <input value={this.state.name} name="name"
                     onChange={this.changeHandler}
                     id="Title" type="text"
              />
              <label htmlFor="Phone">Phone:</label>
              <input value={this.state.phone} name="price"
                     onChange={this.changeHandler}
                     id="Price" type="text"
              />
              <label htmlFor="Email">Email:</label>
              <input value={this.state.email} name="price"
                     onChange={this.changeHandler}
                     id="Price" type="text"
              />
              <label htmlFor="Photo">Photo: </label>
              <input value={this.state.photo} name="imageURL"
                     onChange={this.changeHandler}
                     id="Image" type="text"
              />
              <button className="butn" onClick={this.dishesHandler}>Add To Menu</button>
            </div>
          </form>
        </Fragment>
    );
  }
}

export default EditContact;
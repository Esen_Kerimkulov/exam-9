import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://exam-9-esen.firebaseio.com/'
});

export default instance;